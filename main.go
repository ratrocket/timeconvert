// Convert from local time to UTC.  Useful when your database stores
// date/times in UTC and you're in the db console and want to know "what
// happened on this day?".  Saves doing a "manual" conversion from local
// to UTC.

// Creating this truth table-like thing helped me think about the valid
// combinations of command line flags and given date format.
//
//     bod   eod  dateFmt  OK?  (DO: dateonly; DT: datetime)
//     -----------------------
//      T     T     DO      Y
//      T     F     DO      Y
//      F     T     DO      Y
//      F     F     DO      ? (could be allowed, but isn't currently)
//      T     T     DT      N
//      T     F     DT      N
//      F     T     DT      N
//      F     F     DT      Y
package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
	"runtime"
	"strings"
	"time"
)

const VERSION = "1.0.0"

func main() {
	var (
		dateonly = "2006-01-02"
		datetime = "2006-01-02 15:04:05"
		doRE     = regexp.MustCompile(`^\d{4}-\d{2}-\d{2}$`)
		dtRE     = regexp.MustCompile(`^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$`)
		bod      = flag.Bool("bod", false, "beginning of day for YYYY-MM-DD")
		eod      = flag.Bool("eod", false, "end of day for YYYY-MM-DD")
		showloc  = flag.Bool("showlocal", false, "show local time in output")
		l        = flag.String("l", "America/Los_Angeles", "`location` name (see https://golang.org/pkg/time/#LoadLocation)")
		version  = flag.Bool("version", false, "print long version")
		v        = flag.Bool("v", false, "print short version")
	)
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage of %s\n", os.Args[0])
		fmt.Fprintf(os.Stderr, "  * default behavior: convert local \"YYYY-MM-DD HH:mm:SS\" to UTC\n")
		fmt.Fprintf(os.Stderr, "  * (quoting datetime required for default)\n")
		fmt.Fprintf(os.Stderr, "  * input is (assumed) local, output is UTC\n")
		fmt.Fprintf(os.Stderr, "  * output is in YYYY-MM-DD HH:mm:SS format\n")
		fmt.Fprintf(os.Stderr, "\n")
		flag.PrintDefaults()
		os.Exit(2)
	}
	flag.Parse()

	if *version {
		fmt.Printf("timeconvert version %s %s/%s\n", VERSION, runtime.GOOS, runtime.GOARCH)
		return
	}
	if *v {
		fmt.Println(VERSION)
		return
	}

	loc, err := time.LoadLocation(*l)
	if err != nil {
		log.Print(err)
		flag.Usage()
	}

	arg := strings.TrimSpace(flag.Arg(0))

	if arg == "" || !(doRE.MatchString(arg) || dtRE.MatchString(arg)) {
		flag.Usage()
	}

	if *bod || *eod {
		if !doRE.MatchString(arg) {
			fmt.Fprintf(os.Stderr, "date like YYYY-MM-DD (not %s)\n", arg)
			flag.Usage()
		}
		t, err := time.ParseInLocation(dateonly, arg, loc)
		if err != nil {
			log.Fatal(err)
		}
		if *bod {
			p(datetime, "bod", *showloc, t)
		}
		if *eod {
			t = t.Add(time.Hour * 24).Add(-time.Second)
			p(datetime, "eod", *showloc, t)
		}
		return
	}

	// Just doing a conversion.
	if !dtRE.MatchString(arg) {
		fmt.Fprintf(os.Stderr, "date like \"YYYY-MM-DD HH:mm:SS\" (not %s)\n", arg)
		flag.Usage()
	}
	t, err := time.ParseInLocation(datetime, arg, loc)
	if err != nil {
		log.Fatal(err)
	}
	p(datetime, "", *showloc, t)
}

func p(layout, prefix string, showlocal bool, tLocal time.Time) {
	if prefix != "" && !strings.HasSuffix(prefix, " ") {
		prefix = prefix + " "
	}
	if showlocal {
		fmt.Printf("%sLoc %s\n", prefix, tLocal.Format(layout))
	}
	fmt.Printf("%sUTC %s\n", prefix, tLocal.UTC().Format(layout))
}
