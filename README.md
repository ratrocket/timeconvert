# timeconvert

> **(May 2021)** moved to [md0.org/timeconvert](https://md0.org/timeconvert).

Local -> UTC time conversions.

Default time zone for "local" is Pacific Time (PDT or PST, depending),
a/k/a, "America/Los_Angeles".

# Install

Clone the repository and use `go build` or `go install`

# Output of `timeconvert -help`:

```
Usage of timeconvert
  * default behavior: convert local "YYYY-MM-DD HH:mm:SS" to UTC
  * (quoting datetime required for default)
  * input is (assumed) local, output is UTC
  * output is in YYYY-MM-DD HH:mm:SS format

  -bod
        beginning of day for YYYY-MM-DD
  -eod
        end of day for YYYY-MM-DD
  -l location
        location name (see https://golang.org/pkg/time/#LoadLocation) (default "America/Los_Angeles")
  -showlocal
        show local time in output
  -v    print short version
  -version
        print long version
```
